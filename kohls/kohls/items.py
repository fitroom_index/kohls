# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class KohlsItem(scrapy.Item):
    # define the fields for your item here like:
    itemNum = scrapy.Field()
    category = scrapy.Field()
    itemName = scrapy.Field()
    count = scrapy.Field()
    images = scrapy.Field()
    color = scrapy.Field()
    price = scrapy.Field()
    availability = scrapy.Field()
    size = scrapy.Field()
    inStore=scrapy.Field()
    url = scrapy.Field()
    shortage = scrapy.Field()
    base_url = scrapy.Field()
    crumb = scrapy.Field()
    seller = scrapy.Field()
    features = scrapy.Field()
    geo = scrapy.Field()
    desc = scrapy.Field()
    gender = scrapy.Field()
    timestamp = scrapy.Field()
    regular_price = scrapy.Field()
    hasSale = scrapy.Field()
    brand = scrapy.Field()
