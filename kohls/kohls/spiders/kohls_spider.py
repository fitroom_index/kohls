# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 17:23:06 2016

@author: Andrew
"""

import scrapy
from kohls.items import KohlsItem
import json
import datetime



class KohlsSpider(scrapy.Spider):
    name = "kohls"
    allowed_domains = ["kohls.com"]
    start_urls = ['http://www.kohls.com/feature/brands.jsp']

    

    def parse(self, response):

        #Link to the brand pages
        for sel in response.xpath("//div[@id = 'sitemap-content']/ul/li/a"):
            link = sel.xpath('@href').extract()[0]
            url = response.urljoin(link)
            yield scrapy.Request(url, callback=self.parse_brand_contents)

    #Item Extraction
    def parse_brand_contents(self, response):


        data = response.xpath("//noscript/tr/td/div/a")

       #Check if page has data
        if len(data) == 0:
            pass
        else:
            for sel in response.xpath("//noscript/tr/td/div/a"):
                link = sel.xpath("@href").extract()[0]
                url = response.urljoin(link)
                yield scrapy.Request(url, callback=self.parse_item_contents)
            #recurse onto next page
            next_link = response.xpath("//link[@rel = 'next']/@href").extract()[0]
            new_url = response.urljoin(next_link)
            yield scrapy.Request(new_url, callback=self.parse_brand_contents)

    #information extraction for each item
    def parse_item_contents(self, response):
        ts = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        #json extraction
        for sel in response.xpath("//script[@type = 'text/javascript']"):
            if len(sel.xpath("text()").extract()) != 0 and sel.xpath("text()").extract()[0].find('var productJsonData =')!=-1:
                info = sel.xpath("text()").extract()[0]
        start = info.find('productJsonData = ') + len('productJsonData = ')
        end = info.find('staticContents')-5
        data_set= json.loads(info[start:end]+'}')
        
        #information from JSON
        item_name = data_set['productItem']['productDetails']['displayName']
        product_id = data_set['productItem']['productDetails']['productId']
        sku_table = data_set['productItem']['skuDetails']
        description = data_set['productItem']['accordions']['productDetails']['content']

        #dictionary of items with skuId as the key
        dict_of_items = {}
        for x in sku_table:
            item = KohlsItem()
            item['brand'] = response.xpath("//meta[@itemprop= 'brand']/@content").extract()[0]
            item['features']=""
            item['count'] =""
            item['crumb']=""
            item['timestamp'] = ts
            item['url'] = response.url
            item['seller'] = 'Kohls'
            item['itemName'] = item_name
            item['desc'] = description
            item['category'] = ""
            item['itemNum'] = "Kohls_"+str(x['skuUpcCode'])
            item['color'] = x['color']
            item['size']=x['size2']
            item['availability'] = x['inventoryStatus']
            item['regular_price'] = x['regularPrice']
            item['price'] = x['salePrice']
            if x['regularPrice'] != x['salePrice']:
                item['hasSale'] = 'true'
            else:
                item['hasSale'] = 'false'
            if x['skuId'] in dict_of_items.keys():
                dict_of_items[x['skuId']].append(item)
            else:
                dict_of_items[x['skuId']] = [item]

        #Image Addition
        image_data = response.xpath("//div[@itemtype = 'http://schema.org/Product']")
        for x in image_data:
            sku = x.xpath("meta[@itemprop = 'sku']/@content").extract()[0]
            for y in dict_of_items[sku]:
                y['images'] =[x.xpath("meta[@itemprop = 'image']/@content").extract()[0]]
                yield y





